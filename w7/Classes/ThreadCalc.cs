﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using w7.Logger;

namespace w7.Classes
{
    public class ThreadCalc : ICalculate
    {
        readonly List<Thread> _queueThreads = new ();
        private List<long> _total = new();
        
        public string Calculate(int[] elements)
        {
            var elementInChunk = elements.Count()/Environment.ProcessorCount;

            var chunks = elements.Chunk(elementInChunk);
            foreach (var c in chunks)
            {
                _queueThreads.Add(StartThread(c, true));
            }
            _queueThreads.ForEach(x => x.Join());

            var sum = _total.Sum(x => x);
            return "замер через потоки";
        }

        private Thread StartThread(IEnumerable<int> elem, bool isBackground = false)
        {
            var thread = new Thread(() =>
            {
                _total.Add(elem.Sum(x => (long)x));
            }) { IsBackground = isBackground };
            thread.Start();
            return thread;
        }
    }
}
