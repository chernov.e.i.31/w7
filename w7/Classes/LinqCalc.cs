﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using w7.Logger;

namespace w7.Classes
{
    public class LinqCalc : ICalculate
    {
        public string Calculate(int[] elements)
        {
            var sum = elements.Sum(x => (long)x);
            return "замер через Linq";
        }
    }
}
