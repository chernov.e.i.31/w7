﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace w7.Classes
{
    public interface ICalculate
    {
        string Calculate(int[] count);
    }
}
