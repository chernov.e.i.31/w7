﻿// See https://aka.ms/new-console-template for more information
using System.Diagnostics;
using w7.Classes;
using w7.Logger;

var logger = new ConsoleLog();

var listArr = new List<int[]>{
    Enumerable.Range(1,   100000).ToArray(),
    Enumerable.Range(1,  1000000).ToArray(),
    Enumerable.Range(1, 10000000).ToArray(),
};

var operations = new List<ICalculate>{
    new SimpleCalc(),
    new ThreadCalc(),
    new LinqCalc()
};

var stopWatch = new Stopwatch();

foreach (var item in listArr)
{

    logger.Result("замер " + item.Count());

    foreach (var o in operations)
    {
        stopWatch.Start();
        var res = o.Calculate(item);
        stopWatch.Stop();
        logger.Result(res + " затрачено " + stopWatch.Elapsed.ToString());
        stopWatch.Reset();
    }
}
Console.ReadLine();