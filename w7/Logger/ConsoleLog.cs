﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace w7.Logger
{
    public class ConsoleLog : ILogger
    {
        public void Result(string message)
        {
            Console.WriteLine(message);
        }
    }
}
